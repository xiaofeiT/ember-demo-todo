# Ember Demo - TodoList

[A ember demo]()

## RUN

- git clone 
- npm install 
- npm install -g ember-cli && ember s
  或者 npm run start
- visit [http://localhost:4200](http://localhost:4200)
- visit tests [http://localhost:4200/tests](http://localhost:4200/tests)

## Building

- `ember build` (development)
- `ember build --environment production` (production)

## More

- [ember.js](https://emberjs.com/)
- [ember-cli](https://ember-cli.com/)
- Development Browser Extensions
  - [ember inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)
  - [ember inspector for firefox](https://addons.mozilla.org/en-US/firefox/addon/ember-inspector/)
