import Component from '@ember/component';

export default Component.extend({
    item: '',

    actions: {
        addItem() {
            this.addItem(this.get('item'));
            this.set('item', '');
        }
    }
});
