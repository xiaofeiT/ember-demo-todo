import Component from '@ember/component';

export default Component.extend({

    items: ['1111','22222','33333'],
    
    actions: {
        addItem(item) {
            // 处理item空格
            item = item.replace(/\s+/g,"")
            if (item != ''){
                this.set("items",this.get("items").concat(item));
            }else {
                console.warn('Entry a item.');
            }
        },
        deleteItem(item) {
            // deep copy
            let newItems = [];
            let items = this.get('items');
            for (var i in items) {
                newItems[i] = items[i];
            } 
            let index = newItems.indexOf(item);
            newItems.splice(index,1);
            this.set('items', newItems);
            console.log(this.get('items'));
        }
    }
});