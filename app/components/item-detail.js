import Component from '@ember/component';

export default Component.extend({

    actions: {
        deleteItem(item) {
            this.delete(item);
        }
    }
});